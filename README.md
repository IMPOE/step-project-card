[ # step-project-cards](https://impoe.gitlab.io/-/step-project-card/-/jobs/1264908903/artifacts/public/index.html)

Группа FE-20
Набухотный Роман

[login]: ourproject@mail.ru
[password]: 121212

Сборщик: [GULP]

task run: [npx gulp dev]


___________________________________________________________________________________________________________________________________

    Used Technologies :

    "browser-sync": "^2.26.7",
    ___________________________________________________________________________________________________________________________________
    "del": "^5.1.0",
    ___________________________________________________________________________________________________________________________________
    "gulp": "^4.0.2",
    ___________________________________________________________________________________________________________________________________
    "gulp-autoprefixer": "^7.0.1",
    ___________________________________________________________________________________________________________________________________
    "gulp-concat": "^2.6.1",
    ___________________________________________________________________________________________________________________________________
    "gulp-imagemin": "^7.1.0",
    ___________________________________________________________________________________________________________________________________
    "gulp-postcss": "^8.0.0",
    ___________________________________________________________________________________________________________________________________
    "uncss": "^0.17.3"
    ___________________________________________________________________________________________________________________________________
    "gulp-sass": "^4.1.0",
    ___________________________________________________________________________________________________________________________________
    "gulp-uglify": "^3.0.2",
    ___________________________________________________________________________________________________________________________________
    "gulp-uncss": "^1.0.6",
    ___________________________________________________________________________________________________________________________________
    "postcss": "^7.0.32",
    ___________________________________________________________________________________________________________________________________
    "postcss-uncss": "^0.17.0",
    ___________________________________________________________________________________________________________________________________
    "uncss": "^0.17.3"
    ___________________________________________________________________________________________________________________________________


    [Technical requirements]

The first time a user visits a page, the board should have the inscription No items have been added. The same inscription should be if the user does not have any added cards (for example, he deleted them all).
By clicking on the Login button, a modal window appears in which the user enters his email and password. If it is correct, a list of created visits is displayed to the user on the page.
By clicking on the Create button, a modal window appears, in which you can create a new card.
You need to use ES6 syntax to create classes.
You can use fetch or axios to make AJAX requests.
When you refresh the page or close it, the previously added notes should not disappear.
It is advisable to split the project into modules using ES6 modules.


Modal window "Create visit"
The modal window must contain:

Drop-down list (select) with a choice of a doctor. Depending on the selected doctor, fields will appear under this drop-down list that need to be completed in order to visit this doctor.
There should be three options in the drop-down list - Cardiologist, Dentist, Therapist.
After selecting a doctor from the drop-down list, fields for recording to this doctor should appear below it. Several fields are the same for all three doctors:

purpose of the visit
brief description of the visit
drop-down field - urgency (regular, priority, urgent)
Full name


Also, each of the doctors has their own unique fields to fill out. If the Cardiologist option is selected, the following fields for entering information appear additionally:

normal pressure
body mass index
past diseases of the cardiovascular system
age


If the Dentist option is selected, you must additionally fill in:

date of last visit


If the Therapist option is selected, you must additionally fill in:

age


Create button. When you click on the button, an AJAX request is sent to the corresponding address, and if information about the newly created card is received in the response, a card is created in the Board of visits on the page, the modal window is closed.
Button Close - closes the modal window without saving information and creating a card. By clicking on the area outside the modal window, the modal window is closed.
All input fields, regardless of the selected option, except for the field for additional comments, are required for data entry. Data validation is optional.


[Visit card]
The card, which is created by clicking, appears on the task board. It should look something like this:

It should contain:

Full name that were entered when creating the card
The doctor to whom the person is made an appointment
Show more button. By clicking on it, the card expands, and the rest of the information that was entered when creating a visit appears
Edit button - when you click on it, a drop-down list of what can be done with the card appears:


Edit - instead of the text content of the card, a form appears where you can edit the entered fields. The same as in the modal window when creating a card.

Delete - the card is deleted.




[Visit filters]
You need to do a card filter (an input field for entering search text by title or description of a visit, a drop-down list by status, a drop-down list by priority) you need to do on the front-end - that is, when the value of any form element changes (an item was selected in the drop-down list, something in input) you send a GET request to the server, and, having received the entire array of cards, filter it on the front-end.
Your code should select only the required cards and display them on the screen (like filters in an online store).

[Classes]
JavaScript code must contain the following classes:

Modal class (popup);
Form class (for all forms);
separate classes (components) for form fields (input, select, textarea);
Visit class (describing fields and methods common to all visits to any doctor);
child classes VisitDentist, VisitCardiologist, VisitTherapist;

You need to think over the methods and properties of each class yourself.

Implementation requirements

Any design can be, but it should be.


[AJAX part]
All the necessary documentation for interacting with the AJAX server is here.

